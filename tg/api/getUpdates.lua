local class = require 'metaclass'
local List = require 'metaclass.containers.List'

local getUpdates = class "TG.API.getUpdates" : inherits(require 'tg.validator') : prototype {
	schema = {
		result = List(require 'tg.model.Update')
	},
}

return getUpdates
---@class tg.api.Response:tg.validator
---@field ok boolean
return require "metaclass" "TG.API.Response" : inherits(require 'tg.validator') : prototype {
	schema = {
		ok = 'boolean',
	}
}
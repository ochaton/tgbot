local class = require 'metaclass'

---@class tg.api.SendMessage:tg.validator
---@field public result tg.model.Message
local sendMessage = class "TG.API.sendMessage" : inherits(require 'tg.validator') : prototype {
	schema = {
		result = require 'tg.model.Message'
	},
}

return sendMessage
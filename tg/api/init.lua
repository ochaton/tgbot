local class = require 'metaclass'

local log = require 'log'
local http = require 'http.client'
local json = require 'json'
local fiber = require 'fiber'
local clock = require 'clock'

local Response = require 'tg.api.response'

---@class tg.api:tg.validator
---@field public token string
---@field public timeout number|nil
---@field public mappers table<string, tg.api.Response>
---@field public debug table<string, boolean>
local API = class "TG.API" : inherits(require 'tg.validator') : prototype {
	schema = {
		token = 'string',
	},
	debug = {

	},
	mappers = {
		["getUpdates"] = require 'tg.api.getUpdates',
		["sendMessage"] = require 'tg.api.sendMessage',
		["editMessageText"] = require 'tg.api.editMessageText',
	},
	timeout = 10,
}

---@param method string method name
---@param args table arguments of the request
---@param opts table|nil options of the request (timeout, etc.)
---@return false|tg.api.Response, string|nil
function API:request(method, args, opts)
	opts = opts or {}
	args = args or {}
	assert(method, "method is required for tg.request")

	local url = ("https://api.telegram.org/bot%s/%s"):format(self.token, method)
	local masked = url:gsub("bot([^/]+)", "bot**token**")

	log.verbose("Calling %s with %s", method, json.encode(args))
	local started = clock.time()
	local res = http.post(url, json.encode(args), {
		headers = {
			['content-type'] = 'application/json',
		},
		timeout = opts.timeout or (args.timeout and (args.timeout + 1)) or self.timeout,
	})
	fiber.testcancel()
	local elapsed = clock.time() - started
	log.info("tg.request to %s finished with: %s:%s in %.4f",
		masked, res.status, res.reason, elapsed)

	if self.debug[method] then
		log.info("%s => %s", method, res.body)
	end

	if res.headers and
		res.headers['content-type']
		and res.headers['content-type']:match "^application/json"
	then
		res.body = json.decode(res.body)
	end

	if res.status == 200 then
		return (self.mappers[method] or Response)(res.body)
	else
		log.warn("Sent: %s", json.encode(args))
		log.error("Error: %s", type(res.body) == 'table' and json.encode(res.body) or res.body)
	end

	return false, res.body
end

---@param opts table
---@return tg.model.Update[]
function API:getUpdates(opts)
	return assert(self:request('getUpdates', opts)).result
end

---@param msg table
---@return tg.api.SendMessage
function API:sendMessage(msg)
	return self:request('sendMessage', require 'tg.model.SendMessage'(msg))
end

---@param msg table
---@return tg.api.editMessageText
function API:editMessageText(msg)
	return self:request('editMessageText', require 'tg.model.editMessageText'(msg))
end

---@param file table
---@return tg.api.Response
function API:getFile(file)
	local file_id = assert(file.file_id, "need file_id")

	local file_info = assert(self:request('getFile', {file_id = file_id}))
	---@cast file_info tg.api.Response
	if not file_info.ok then
		return file_info
	end

	local r = http.get(('https://api.telegram.org/file/bot%s/%s'):format(self.token, file_info.file_path))
	if r.status ~= 200 then
		return { ok = false, error = "http status not 200:"..r.status }
	end

	return { ok = true, file = r.body, info = file_info, content_type = r.headers['content-type'] }
end

function API:answerInlineQuery(answer)
	return self:request('answerInlineQuery', answer)
end

function API:answerCallbackQuery(answer)
	return self:request('answerCallbackQuery', answer)
end

return API

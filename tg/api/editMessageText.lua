local class = require 'metaclass'

---@class editMessageText:tg.validator
---@field result tg.model.Message
local editMessageText = class "TG.API.editMessageText" : inherits(require 'tg.validator') : prototype {
	schema = {
		result = require 'tg.model.Message'
	},
}

return editMessageText
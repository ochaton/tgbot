local class = require 'metaclass'
local fiber = require 'fiber'
local log = require 'log'
local ev = require 'tg.ev'
local tgapi = require 'tg.api'
local FiberPool = require 'tg.fpool'.pool

---@class tg.bot:tg.validator
---@field public tgapi tg.api
---@field public token string
---@field public timeout number|nil
---@field public auto_offset boolean|nil
---@field public pool FiberPool
---@field public debug table<string,boolean>
---@field private bus tg.ev
local Bot = class "TG.Bot":inherits(require 'tg.validator'):prototype {
	schema = {
		token = 'string',
		timeout = '?number',
		auto_offset = '?boolean',
	},

	timeout = 3,
	auto_offset = true,
	offset = 0,

	debug = {},

	pool = {
		size = 32,
		timeout = 3,
	},
}

function Bot:constructor()
	self.fpool = FiberPool { size = self.pool.size, timeout = self.pool.timeout }
	self.bus = ev {}
	self.tgapi = tgapi { token = self.token, debug = self.debug }
	self:setup()
end

function Bot.setup()
	-- do nothing
end

---@param what string event namespace
---@param func function event handler
function Bot:on(what, func)
	self.bus:on(what, function(_, ...) return func(self, ...) end)
end

function Bot:run()
	self.fiber = fiber.new(function(bot)
		fiber.name("bot/loop")
		while fiber.self() == bot.fiber do
			local ok, err = xpcall(bot.worker_f, debug.traceback, bot)
			if not ok then
				log.error("bot.worker_f failed: %s", err)
			end
			fiber.sleep(1)
		end
		log.warn("Leaving fiber")
	end, self)
end

---@param u tg.model.Update
function Bot:process(u)
	local ok = self.bus:any('update', u)
	if not ok then
		log.warn("noone caught update")
	end

	if self.auto_offset then
		self.offset = math.max(self.offset, u.update_id)
	end
end

function Bot:worker_f()
	local us = self.tgapi:getUpdates {
		timeout = self.timeout,
		limit = self.fpool:left(),
		offset = self.offset + 1,
	}

	for _, u in ipairs(us) do
		u:setAPI(self.tgapi)
		self.fpool:run(self.process, self, u)
			:on_fail(self.on_fail, self)
	end
end

function Bot:on_fail(err) --luacheck: ignore
	-- do nothing
end

return Bot

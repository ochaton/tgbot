local class = require 'metaclass'
local LinkedList = require 'metaclass.containers.LinkedList'

---@class tg.ev event bus
---@field private handlers {string:fun(bus: tg.ev, ...:any):any[]}
---@field private namespaces {string:{string:fun(bus: tg.ev, ...:any):any[]}}
local Events = class "Events"

function Events:constructor()
	self.handlers = {}
	self.namespaces = {}
end

function Events:_register_handler(name, func)
	self.handlers[name] = self.handlers[name] or LinkedList()
	local hs = self.handlers[name]

	return hs:push(func)
end

function Events:_register_namespace(ns, event, ref)
	self.namespaces[ns] = self.namespaces[ns] or {}
	ns = self.namespaces[ns]
	ns[event] = ns[event] or {}
	table.insert(ns[event], ref)
end

---@param name string event namespace
---@param func fun(bus: tg.ev, ...:any):any event handler
function Events:on(name, func)
	local ns
	name, ns = name:match("^(.+)%.([^.]+)$")
	assert(name, "invalid name of event")

	local ref = self:_register_handler(name, func)
	if ns then
		self:_register_namespace(ns, name, ref)
	end
end

---@param nsn string namespace
function Events:off(nsn)
	if not self.namespaces[nsn] then return end
	local ns = self.namespaces[nsn]

	for event_name in pairs(ns) do
		local event_list = self.handlers[event_name]
		for _, ref in ipairs(ns[event_name]) do
			event_list:remove(ref)
		end
		if event_list:empty() then
			self.handlers[event_name] = nil
		end
	end
	self.namespaces[nsn] = nil
	return true
end

function Events:_fire(how, name, ...)
	local e = {n = select('#', ...), ...}
	if not self.handlers[name] then return false, "no route" end

	local r = {}
	for _, f in self.handlers[name]:pairs() do
		local ok, ret = pcall(f, self, unpack(e, 1, e.n))
		if not ok then
			error(ret, 2)
		end

		if how == 'any' then
			if ret then return ret end
		elseif how == 'all' then
			if not ret then return false, "handler failed" end
			table.insert(r, ret)
		end
	end
	return r
end

---Returns first result of successfull Event Handler
---@param name string event name
---@param ... any arguments of the event
---@return any
function Events:any(name, ...)
	return self:_fire('any', name, ...)
end

---Returns results of all event handlers
---@param name string event name
---@param ... any arguments of the event
---@return any[]
---@return false, string
function Events:all(name, ...)
	return self:_fire('all', name, ...)
end

return Events

--[[
e:on('update.chat', function() return "update.chat" end)
e:on('message.command', function() return "message.command" end)
e:on('update.message', function() return "update.message" end)
e:on("message.chat", function() return "message.chat" end)
]]
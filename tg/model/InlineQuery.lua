local class = require 'metaclass'
local User = require 'tg.model.User'

---@class tg.model.InlineQuery:tg.model.base
---@field public id string
---@field public chat_type string
---@field public query string|nil
---@field public offset string|nil
---@field public from tg.model.User|nil
local InlineQuery = class "TG.InlineQuery" : inherits(require 'tg.model.base') : prototype {
	schema = {
		id = 'string',
		chat_type = 'string',
		from = { opt = true, class = User },
		query = '?string',
		offset = '?string',
	},
}

return InlineQuery
local class = require 'metaclass'
local Message = require 'tg.model.Message'
local CallbackQuery = require 'tg.model.CallbackQuery'
local InlineQuery = require 'tg.model.InlineQuery'

---@class tg.model.Update:tg.model.base
---@field public update_id number
---@field public message tg.model.Message|nil
---@field public edited_message tg.model.Message|nil
---@field public callback_query tg.model.CallbackQuery|nil
local Update = class "TG.Update" : inherits(require 'tg.model.base') : prototype {
	schema = {
		update_id = 'number',
		message        = { opt = true, class = Message },
		edited_message = { opt = true, class = Message },
		callback_query = { opt = true, class = CallbackQuery },
		inline_query   = { opt = true, class = InlineQuery },
	},
}

function Update:setAPI(api)
	assert(api:isa(require 'tg.api'), "api must be tg.api")
	self._api = api
end

function Update:sendMessage(msg)
	assert(self._api, "_api is not set")
	if not msg.chat_id then
		msg.chat_id = self.message.chat.id or self.message.from.id
	end
	return self._api:sendMessage(msg)
end

function Update.getSchema(space)
	Update.methods._space = space
	return {
		class = Update,
		format = {
			{ name = 'update_id',      type = 'number' },
			{ name = 'message',        type = Message.methods.schema.message_id, ref = 'messages', is_nullable = true },
			{ name = 'edited_message', type = Message.methods.schema.message_id, ref = 'messages', is_nullable = true },
			{ name = 'callback_query', type = CallbackQuery.methods.schema.message_id, ref = 'callbacks', is_nullable = true },
		},
		primary = {'update_id'},
	}
end

function Update:persist(spaces)
	local r = table.deepcopy(self)
	if self.message then
		r.message = self.message:persist(spaces)
	end
	if self.edited_message then
		r.edited_message = self.edited_message:persist(spaces)
	end
	if self.callback_query then
		r.callback_query = self.callback_query:persist(spaces)
	end
	return spaces.updates:replace(r)
end

return Update

local class = require 'metaclass'

---@class tg.model.base:tg.validator
local Base = class "TG.Base" : inherits(require 'tg.validator')

function Base:constructor()
	if self._space then
		self:unfold()
	end
	Base.parent.constructor(self)
end

return Base
local class = require 'metaclass'

---@class tg.model.Chat:tg.model.base
---@field public id number
---@field public type string
---@field public title string|nil
---@field public username string|nil
local Chat = class "TG.Chat" : inherits(require 'tg.model.base') : prototype {
	schema = {
		id = 'number',
		type = 'string',
		title = '?string',
		username = '?string',
	},
}

function Chat.getSchema(space)
	Chat.methods._space = assert(space, "space not defined")
	return {
		class = Chat,
		format = {
			{ name = 'id',       type = 'number' },
			{ name = 'type',     type = 'string' },
			{ name = 'username', type = 'string', is_nullable = true },
			{ name = 'title',    type = 'string', is_nullable = true },
		},
		primary = {'id'},
		secondaries = {
			{ name = 'username', parts = {'username'} },
		}
	}
end

function Chat:persist(spaces)
	return spaces.chats:replace(self)
end


return Chat
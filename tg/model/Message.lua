local class = require 'metaclass'
local List = require 'metaclass.containers.List'
local User = require 'tg.model.User'
local Chat = require 'tg.model.Chat'

---@class tg.model.Message:tg.model.base
---@field public message_id number
---@field public from tg.model.User|nil
---@field public date number
---@field public chat tg.model.Chat
---@field public text string|nil
---@field public entities tg.model.MessageEntity[]|nil
local Message = class "TG.Message" : inherits(require 'tg.model.base'): prototype {
	schema = {
		message_id = 'number',
		from       = { opt = true, class = User },
		date       = 'number',
		chat       = Chat,
		text       = '?string',
		entities   = { opt = true, class = List(require 'tg.model.MessageEntity') },
	}
}

function Message.getSchema(space)
	Message.methods._space = space
	return {
		class = Message,
		format = {
			{ name = 'message_id', type = 'number' },
			{ name = 'date',       type = 'number' },
			{ name = 'chat',       type = Chat.methods.schema.id, ref = 'chats' },
			{ name = 'from',       type = User.methods.schema.id, ref = 'users', is_nullable = true },
			{ name = 'text',       type = 'string', is_nullable = true },
			{ name = 'entities',   type = 'any', is_nullable = true },
			{ name = 'photo',      type = 'any', is_nullable = true },
			{ name = 'document',   type = 'any', is_nullable = true },
			{ name = 'reply_to_message', type = Message.methods.schema.id, ref = 'messages', is_nullable = true },
		},
		primary = {'message_id'},
		secondaries = {
			{ name = 'chat', parts = {'chat', 'message_id'} },
			{ name = 'date', parts = {'date', 'message_id'} },
		}
	}
end

function Message:persist(spaces)
	local r = table.deepcopy(self)
	if self.chat then
		r.chat = self.chat:persist(spaces)
	end
	if self.from then
		r.from = self.from:persist(spaces)
	end
	return spaces.messages:replace(r)
end

return Message
local class = require 'metaclass'
local List  = require 'metaclass.containers.List'

---@class tg.model.SendMessage:tg.validator
---@field public chat_id number
---@field public text string
---@field public parse_mode string|nil
---@field public reply_to_message_id string|nil
---@field public entities tg.model.MessageEntity[]|nil
---@field public reply_markup tg.model.Keyboard|nil
local SendMessage = class "TG.SendMessage" : inherits(require 'tg.validator') : prototype {
	schema = {
		chat_id             = 'number',
		text                = 'string',
		parse_mode          = '?string',
		reply_to_message_id = '?string',
		entites             = { opt = true, class = List(require 'tg.model.MessageEntity') },
		-- reply_markup        = { opt = true, class = List(require 'tg.model.Keyboard') },
	},
}

return SendMessage
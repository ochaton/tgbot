local class = require 'metaclass'

---@class tg.model.MessageEntity:tg.model.base
---@field public type string
---@field public offset number
---@field public length number
local Message = class "TG.MessageEntity" : inherits(require 'tg.model.base') : prototype {
	schema = {
		type = 'string',
		offset = 'number',
		length = 'number',
	}
}

return Message
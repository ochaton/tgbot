local class = require 'metaclass'
local List = require 'metaclass.containers.List'


local InlineKeyboardButton = class "InlineKeyboardButton":inherits(require 'tg.model.base'):prototype {
	schema = {
		text = 'string',
		callback_data = '?string',
		switch_inline_query = '?string',
		switch_inline_query_current_chat = '?string',
	},
}

local KeyboardButton = class "KeyboardButton":inherits(require 'tg.model.base'):prototype {
	schema = {
		text = 'string',
	},
}

local InlineKeyboardMarkup = class "InlineKeyboardMarkup":inherits(require 'tg.model.base'):prototype {
	schema = {
		inline_keyboard = { class = List(List(InlineKeyboardButton)) },
	},
}

local ReplyKeyboardMarkup = class "ReplyKeyboardMarkup":inherits(require 'tg.model.base'):prototype {
	schema = {
		keyboard = { class = List(List(KeyboardButton)) },
		resize_keyboard = '?boolean',
		one_time_keyboard = '?boolean',
		selective = '?boolean',
	},
}

local KeyboardMarkup = class "KeyboardMarkup"

function KeyboardMarkup:constructor()
	if self.inline_keyboard then
		return InlineKeyboardMarkup(self)
	elseif self.keyboard then
		return ReplyKeyboardMarkup(self)
	end
end

function KeyboardMarkup:isa(need)
	if type(need) == 'table' then
		need = need:name()
	end
	if need ~= 'KeyboardMarkup' then
		return self:parent().methods.isa(self, need)
	end
	if self:isa('ReplyKeyboardMarkup') or self:isa('InlineKeyboardMarkup') then
		return true
	end
	return false
end

return KeyboardMarkup

local class = require 'metaclass'
local List  = require 'metaclass.containers.List'

return class "TG.editMessageText" : inherits(require 'tg.validator') : prototype {
	schema = {
		chat_id             = 'number',
		message_id          = '?number',
		inline_message_id   = '?string',
		text                = 'string',
		parse_mode          = '?string',
		entites             = { opt = true, class = List(require 'tg.model.MessageEntity') },
	},
}

local class = require 'metaclass'
local User = require 'tg.model.User'
local Message = require 'tg.model.Message'

---@class tg.model.CallbackQuery:tg.model.base
---@field public id string
---@field public chat_instance string
---@field public inline_message_id string|nil
---@field public from tg.model.User|nil
---@field public message tg.model.Message|nil
local CallbackQuery = class "TG.CallbackQuery" : inherits(require 'tg.model.base') : prototype {
	schema = {
		id = 'string',
		chat_instance = 'string',
		from = { opt = true, class = User },
		inline_message_id = '?string',
		message = { opt = true, class = require 'tg.model.Message' },
	},
}

function CallbackQuery.getSchema(space)
	CallbackQuery.methods._space = space
	return {
		class = CallbackQuery,
		format = {
			{ name = 'id',                type = 'string' },
			{ name = 'chat_instance',     type = 'string' },
			{ name = 'inline_message_id', type = 'string', is_nullable = true },
			{ name = 'from',              type = User.methods.schema.id, ref = 'users', is_nullable = true },
			{ name = 'message',           type = Message.methods.schema.message_id, ref = 'messages', is_nullable = true },
		},
		primary = {'id'},
		secondaries = {
			{ name = 'from', parts = {'from', 'id'} },
		},
	}
end

function CallbackQuery:persist(spaces)
	local r = table.deepcopy(self)
	if self.from then
		r.from = self.from:persist(spaces)
	end
	if self.message then
		r.message = self.message:persist(spaces)
	end
	return spaces.callbacks:replace(r)
end

return CallbackQuery
local class = require 'metaclass'

---@class tg.model.User:tg.model.base
---@field public id number
---@field public is_bot boolean
---@field public first_name string
---@field public last_name string|nil
---@field public username string|nil
---@field public language_code string|nil
---@field public can_join_groups boolean|nil
---@field public can_read_all_group_messages boolean|nil
local User = class "TG.User" : inherits(require 'tg.model.base') : prototype {
	schema = {
		id                          = 'number',
		is_bot                      = 'boolean',
		first_name                  = 'string',
		last_name                   = '?string',
		username                    = '?string',
		language_code               = '?string',
		can_join_groups             = '?boolean',
		can_read_all_group_messages = '?boolean',
	}
}

function User.getSchema(space)
	User.methods._space = space
	return {
		class = User,
		format = {
			{ name = 'id',                          type = 'number'                      },
			{ name = 'is_bot',                      type = 'boolean'                     },
			{ name = 'first_name',                  type = 'string'                      },
			{ name = 'last_name',                   type = 'string',  is_nullable = true },
			{ name = 'username',                    type = 'string',  is_nullable = true },
			{ name = 'language_code',               type = 'string',  is_nullable = true },
			{ name = 'can_join_groups',             type = 'boolean', is_nullable = true },
			{ name = 'can_read_all_group_messages', type = 'boolean', is_nullable = true },
		},
		primary = {'id'},
		secondaries = {
			{ name = 'username', parts = {'username'} },
		}
	}
end

function User:persist(spaces)
	return spaces.users:replace(self)
end

return User
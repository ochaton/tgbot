local class = require 'metaclass'
local fiber = require 'fiber'
local log   = require 'log'

local function pack(ok, ...)
	return ok, { n = select('#', ...), ... }
end

local Future = class "Future"

function Future:constructor()
	self.deadline = self.deadline or math.huge
end

function Future:signal(r)
	self.chan:close()
	if r.ok then
		return unpack(r.result, 1, r.result.n)
	end

	if self._on_fail_cb then
		self._on_fail_cb(unpack(self._on_fail_args, 1, self._on_fail_args.n))
	else
		error(r.result[1], 2)
	end
end

function Future:get()
	local r = self.chan:get(self.deadline - fiber.time())

	if not r then
		box.error(box.error.TIMEOUT)
	end

	self:signal(r)
end

function Future:pcall_get()
	return pcall(self.get, self)
end

---Sets on_fail callback and make Future async
---@param ... unknown
function Future:on_fail(...)
	local cb, args = pack(...)

	self._async = true
	self._on_fail_cb = cb
	self._on_fail_args = args

	return self
end


local FiberPool = class "FiberPool"

---@class FiberPool
---@field public size number length of fiber pool
---@field public timeout number timeout of task execution
---@field public on_the_fly number amount of currently running tasks

function FiberPool:constructor()
	self.size = self.size or 1
	self.timeout = self.timeout or 1
	self.tasks = fiber.channel(self.size)
	self.result = fiber.channel(self.size)
	self.finished = fiber.channel(self.size)
	self.on_the_fly = 0

	for w = 1, self.size do
		fiber.create(self.fiber_f, self, w)
	end
end

function FiberPool:fiber_f(no)
	fiber.name("fpool/"..no)
	while not self.tasks:is_closed() do
		local task = self.tasks:get()
		if task then
			local fut = task.fut
			self.on_the_fly = self.on_the_fly + 1
			local ok, res = pack(pcall(task.func, unpack(task.args, 1, task.args.n)))
			self.on_the_fly = self.on_the_fly - 1
			if not ok then
				log.error("Execution failed with: %s", res[1])
			end

			local msg = { ok = ok, result = res }

			fiber.testcancel()
			if fut._async then
				local sig_ok, err = pcall(fut.signal, fut, msg)
				if not sig_ok then
					log.error("Future:signal failed with %s", err)
				end
			else
				if task.cb:has_readers() then
					task.cb:put(msg)
				else
					log.error("future does not have readers")
				end
			end

			fiber.testcancel()
		end
	end
	self.finished:put(no)
end

function FiberPool:close(timeout)
	timeout = timeout or 60
	local deadline = fiber.time()+timeout
	self.tasks:close()

	local finished = 0
	for _ = 1, self.size do
		if deadline <= fiber.time() then break end
		local r = self.finished:get(deadline-fiber.time())
		if not r then break end

		log.warn("Worker %s finished", r)
		finished = finished+1
	end

	self.finished:close()

	if finished == self.size then
		log.info("All workers was successfully stopped")
	else
		log.warn("Some workers continued to run. Stop them with force")
		for _, f in ipairs(self.fibers) do
			pcall(fiber.cancel, f)
		end
	end

	return true
end

function FiberPool:left()
	return math.max(self.size - self.on_the_fly, 0)
end

function FiberPool:run(...)
	local func, args = pack(...)
	local deadline = fiber.time() + self.timeout

	local chan = fiber.channel()
	local fut = Future { deadline = deadline, chan = chan }
	local task = setmetatable({ func = func, args = args, cb = chan, fut = fut }, { __mode = 'v' })

	self.tasks:put(task, deadline-fiber.time())

	return fut
end

return {
	pool = FiberPool,
	future = Future,
}
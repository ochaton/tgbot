local orm = require 'orm' { create_if_not_exists = true }
package.loaded[...] = orm
rawset(_G, 'orm', orm)

orm:configure {
	tasks = {
		class = require 'tg.queue.task',
		format = {
			{ name = 'id',       type = 'unsigned', auto = 'time64'     },
			{ name = 'dedup',    type = 'string',   is_nullable = true  },
			{ name = 'status',   type = 'string',                       },
			{ name = 'runat',    type = 'unsigned',                     },
			{ name = 'group',    type = 'string',   is_nullable = true  },
			{ name = 'tube',     type = 'string',   is_nullable = true  },
			{ name = 'pri',      type = 'unsigned',                     },
			{ name = 'mtime',    type = 'unsigned',                     },
			{ name = 'attempts', type = 'unsigned',                     },
			{ name = 'payload',  type = '*'                           },
			{ name = 'error',    type = '*',      is_nullable = true  },
		},

		primary = {'id'},
		secondaries = {
			{ name = 'take',  parts = {'status', 'pri', 'id'} },
			{ name = 'tube',  parts = {'tube', 'status', 'pri', 'id'} },
			{ name = 'group', parts = {'group', 'status', 'id'} },
			{ name = 'runat', parts = {'runat', 'id'} },
			{ name = 'dedup', parts = {'dedup'} },
		},
	},
}

return orm
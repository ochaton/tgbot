local class = require 'metaclass'
local decimal = require 'decimal'
local uuid = require 'uuid'
local ffi = require 'ffi'

---@class tg.validator:metaclass.Object
local Validator = class "Validator" : inherits(require 'orm.Tuple')

local type_casters = {
	boolean = function(v)
		if v == 'true' then
			return true
		elseif v == 'false' then
			return false
		end
		error(("Cannot cast %s to boolean"):format(v))
	end,

	number = function(v)
		return (assert(tonumber(v), ("Cannot cast %s to number"):format(v)))
	end,

	string = tostring,
}

local function init(base_type, val)
	if base_type == type(val) then
		return val
	end
	if type_casters[base_type] then
		return type_casters[base_type](val)
	end
	if type(base_type) == 'table' and base_type.__name then
		return base_type(val)
	end
	error(("Cannot initialize %s for %s"):format(base_type, val))
end

local function type_satisfied(value, need_type)
	local btype = type(value)
	if btype == need_type then
		return true, value
	end

	if need_type == 'any' then
		return true, value
	end

	if need_type == 'array' or need_type == 'map' then
		if type(btype) == 'table' then
			return true, value
		end
		return false, btype
	end

	if need_type == 'uuid' then
		if btype == 'string' then
			local v = uuid.fromstr(btype)
			if v == nil then
				return false, btype
			end
			return true, v
		elseif btype == 'cdata' then
			if ffi.istype("struct tt_uuid", value) then
				return true, value
			end
			return false, tostring(ffi.typeof(value))
		end
		return false, btype
	end

	if need_type == 'decimal' then
		if btype == 'cdata' and decimal.is_decimal(value) then
			return true
		else
			local ok, v = pcall(decimal.new, value)
			if ok then
				return true, v
			end
		end
		return false, btype
	end

	if need_type == 'number' or need_type == 'integer' then
		if tonumber(value) then
			return true, tonumber(value)
		end
		return false, btype
	end

	if type(need_type) == 'table' and need_type.__name then
		return true, need_type(value)
	end
end

local function validate(obj, field, val)
	local opt, def, need_type
	if type(val) ~= 'string' then
		assert(type(val) == 'table', "invalid type info for "..tostring(field))
		if val.__name then
			need_type = val
		elseif val.class then
			opt = val.opt
			def = val.def
			need_type = val.class
		else
			error("NOT IMPLEMENTED")
		end
	else
		-- opt:
		if val:sub(1, 1) == '?' then
			opt = true
		end
		do -- def:
			local pos = val:find(':')
			if pos then
				def = val:sub(pos+1)
			end
		end
		need_type = val:match("^%??([^:]+)")
	end

	if obj[field] == nil then
		if not opt then
			return false, "required field is not given"
		end
		if def then
			obj[field] = init(need_type, def)
		end
		return true
	end

	-- obj[field] != nil:
	local ok, real_type_or_value = type_satisfied(obj[field], need_type)
	if not ok then
		return false, ("required to be %s got %s"):format(need_type.__name or need_type, real_type_or_value)
	else
		obj[field] = real_type_or_value
	end

	return true
end

local json = require 'json'

function Validator:constructor()
	assert(self:who() ~= "Validator", "You can't instantiate Validator object: it is abstract")

	-- validate schema:
	local errors = {}
	for fname, val in pairs(self.schema) do
		local ok, err = validate(self, fname, val)
		if not ok then
			table.insert(errors, ("%s: %s"):format(fname, err))
		end
	end

	if next(errors) then
		error(json.encode(errors), 0)
	end
end

function Validator:extendSchema(extend)
	for f, v in pairs(self.schema) do
		if extend[f] == nil then
			extend[f] = v
		end
	end
	return extend
end

return Validator